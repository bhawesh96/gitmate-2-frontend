import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArraySettingComponent } from './array-setting.component';

describe('ArraySettingComponent', () => {
  let component: ArraySettingComponent;
  let fixture: ComponentFixture<ArraySettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArraySettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArraySettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
